﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using ZXing;
using ZXing.Common;

namespace QrCodeCapture.ImageServices
{
    public class QrCodeService
    {
        public static Result QrCodeCaptured(BitmapSource image)
        {
            if (image != null)
            {
                Result result = null;

                var bitmap = BitmapService.BitmapFromSource(image);
                var arr = BitmapService.BitmapToByte(bitmap);

                var reader = new BarcodeReader()
                {
                    AutoRotate = true,
                    TryInverted = true
                };

                try
                {
                    result = reader.Decode(bitmap);
                }
                catch { }

                return result;
            }

            return null;
        }
    }
}
