﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.UI;
using QrCodeCapture.ImageServices;

namespace QrCodeCapture
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ImageViewer mImageViewer;
        public MainWindow()
        {
            InitializeComponent();

            RunCamera();
        }

        private void RunCamera()
        {
            mImageViewer = new ImageViewer();
            var capture = new Capture();

            MainImage.MouseLeftButtonDown += new MouseButtonEventHandler(CaptureCode);

            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    mImageViewer.Image = capture.QueryFrame();

                    try
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MainImage.Source = BitmapService.ToBitmapSource(mImageViewer.Image);
                        });
                    }
                    catch { }
                }
            });
        }

        private void CaptureCode(object sender, MouseButtonEventArgs e)
        {
            var result = QrCodeService.QrCodeCaptured(BitmapService.ToBitmapSource(mImageViewer.Image));

            if (result != null)
            {
                MessageBox.Show(result.Text);
            }
        }
    }
}
